VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmTran850xml 
   Caption         =   "Translates EDI to XML or XML to EDI"
   ClientHeight    =   3630
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6285
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   9
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   3630
   ScaleWidth      =   6285
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdEDI2XML 
      Caption         =   "Translate EDI X12 to XML"
      Height          =   735
      Left            =   3600
      TabIndex        =   9
      Top             =   2880
      Width           =   2655
   End
   Begin VB.OptionButton optXML2EDI 
      Caption         =   "XML to EDI"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2520
      TabIndex        =   8
      Top             =   2160
      Width           =   1935
   End
   Begin VB.OptionButton optEDI2XML 
      Caption         =   "EDI to XML"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   7
      Top             =   2160
      Width           =   2055
   End
   Begin VB.CommandButton cmdattachXMLfile 
      Caption         =   "Attach XML file"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   6
      Top             =   1440
      Width           =   1815
   End
   Begin VB.TextBox txtXMLfileName 
      Height          =   495
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   1440
      Width           =   4335
   End
   Begin VB.CommandButton cmdattachEDIfile 
      Caption         =   "Attach EDI file"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   4
      Top             =   840
      Width           =   1815
   End
   Begin VB.TextBox txtEDIfileName 
      Height          =   495
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   840
      Width           =   4335
   End
   Begin VB.CommandButton cmdattachSEFfile 
      Caption         =   "Attach SEF file"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4440
      TabIndex        =   2
      Top             =   240
      Width           =   1815
   End
   Begin VB.TextBox txtSEFfileName 
      Height          =   495
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   240
      Width           =   4335
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5760
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdXML2EDI 
      Caption         =   "Translate XML to EDI X12"
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   2880
      Width           =   2655
   End
End
Attribute VB_Name = "frmTran850xml"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************************************
'*  Translates an EDI X12 850 from an XML file.
'*  EDI filename: 850.x12.xml
'*  Required SEF filename: 850_4010.SEF
'********************************************************************************************
Option Explicit
Private oEdiDoc As Fredi.ediDocument
Private oSegment As Fredi.ediDataSegment
Private oSchemas As Fredi.ediSchemas
Private oXmlDoc As Fredi.xmlDocument

Private mPath As String
Private mSEFfilename As String
Private mEDIfilename As String
Private mXMLfilename As String

Private Sub cmdattachEDIfile_Click()
    CommonDialog1.Filter = "Apps (*.X12)|*.X12|All files (*.*)|*.*"
    CommonDialog1.DefaultExt = "X12"
    CommonDialog1.InitDir = mPath
    CommonDialog1.DialogTitle = "Select File EDI(X12) File"
    CommonDialog1.ShowOpen
    
    mEDIfilename = CommonDialog1.FileName
    txtEDIfileName.Text = mEDIfilename
End Sub

Private Sub cmdattachSEFfile_Click()
    
    CommonDialog1.Filter = "Apps (*.SEF)|*.SEF|All files (*.*)|*.*"
    CommonDialog1.DefaultExt = "SEF"
    CommonDialog1.InitDir = mPath
    CommonDialog1.DialogTitle = "Select File SEF File"
    CommonDialog1.ShowOpen
    
    mSEFfilename = CommonDialog1.FileName
    txtSEFfileName.Text = mSEFfilename
    
End Sub

Private Sub cmdattachXMLfile_Click()
    
    CommonDialog1.Filter = "Apps (*.XML)|*.XML|All files (*.*)|*.*"
    CommonDialog1.DefaultExt = "XML"
    CommonDialog1.InitDir = mPath
    CommonDialog1.DialogTitle = "Select File SEF File"
    CommonDialog1.ShowOpen
    
    mXMLfilename = CommonDialog1.FileName
    txtXMLfileName.Text = mXMLfilename
    
End Sub

Private Sub cmdEDI2XML_Click()
    'This sample program is an example of how one can programmatically translate an EDI file into an XML file.
    'This program has an advantage over the FREDI built-in function in that it is more memory effecient, and
    'allows you more control of the XML format output
    
    If Trim(mEDIfilename) = "" Or Trim(mSEFfilename) = "" Then
        MsgBox "Please attach Required files", vbOKOnly
    Else
        Dim oEdiDoc As Fredi.ediDocument
        Dim oSchemas As Fredi.ediSchemas
        Dim oSegment As Fredi.ediDataSegment
        Dim oElement As Fredi.ediDataElement
        Dim oComposite As Fredi.ediDataElements
        Dim oSubElement As Fredi.ediDataElement
        
        Dim oXmlFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        
        Dim sSegmentId  As String
        Dim nElemCount As Integer
        Dim i As Integer
        
        If Trim(mXMLfilename) = "" Then
            mXMLfilename = "Output_XmlFile.xml"
        End If
        
        Set oFs = New Scripting.FileSystemObject
        Set oXmlFile = oFs.CreateTextFile(mXMLfilename, True)
        oXmlFile.Write "<?xml version=""1.0"" encoding=""ISO-8859-1"" ?>"
        
        Set oEdiDoc = New Fredi.ediDocument
        
        'Changing the cursor type to forward does not load the entire EDI file at once,
        'but at data segments at a time and disposing of them after being read, thus is more
        'memory effecient
        oEdiDoc.CursorType = Cursor_ForwardOnly
        
        'Disable internal library, and use SEF files only.
        Set oSchemas = oEdiDoc.GetSchemas
        oSchemas.EnableStandardReference = False
        
        'Load SEF file
        oEdiDoc.LoadSchema mSEFfilename, Schema_Standard_Exchange_Format
        
        'Load EDI File
        oEdiDoc.LoadEdi mEDIfilename
        
        oXmlFile.Write "<EDI-X12-4010>"
        'oXmlFile.Write ""<Events xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="214_Schema.xsd">""
        Set oSegment = oEdiDoc.FirstDataSegment
        Do While Not oSegment Is Nothing
            sSegmentId = oSegment.Id
            
            'Write open tag for control segments
            If sSegmentId = "ISA" Then
                oXmlFile.Write "<Interchange-Control>"
            ElseIf sSegmentId = "GS" Then
                oXmlFile.Write "<Functional-Group-Control>"
            ElseIf sSegmentId = "ST" Then
                oXmlFile.Write "<Transaction-Set-Control>"
            End If
        
            'Write open segment reference information
            oXmlFile.Write "<SegmentRef ID=""" & Trim(oSegment.Id) & """ Pos=""" & Trim(Str(oSegment.Position)) & """>"
        
            nElemCount = oSegment.Count
            For i = 1 To nElemCount
            Set oElement = oSegment.DataElement(i)
                'Write element information
                oXmlFile.Write "<Element Id=""" & oElement.Id & """ Pos=""" & Trim(Str(oElement.Position)) & """ Value=""" & Trim(oElement.Value) & """/>"
            Next
        
            'Write close tag for segment reference
            oXmlFile.Write "</SegmentRef>"
            
            'Write close tag for control segments
            If sSegmentId = "SE" Then
                oXmlFile.Write "</Transaction-Set-Control>"
            ElseIf sSegmentId = "GE" Then
                oXmlFile.Write "</Functional-Group-Control>"
            ElseIf sSegmentId = "IEA" Then
                oXmlFile.Write "</Interchange-Control>"
            End If
            
            Set oSegment = oSegment.Next
            
        Loop
        
        oXmlFile.Write "</EDI-X12-4010>"
    
        oXmlFile.Close
        
        MsgBox "Done. File output = " + mXMLfilename
    
    End If
End Sub

Private Sub cmdXML2EDI_Click()
    
    If Trim(mXMLfilename) = "" Or Trim(mSEFfilename) = "" Then
        MsgBox "Please attach Required files", vbOKOnly
    Else
        Dim oEdiDoc As Fredi.ediDocument
        Set oEdiDoc = New Fredi.ediDocument
        
        'Makes certain that the internal Standard Reference Library will not be used, but
        'only uses the SEF file provided.
        Set oSchemas = oEdiDoc.GetSchemas
        oSchemas.EnableStandardReference = False
        
        oEdiDoc.ImportSchema mSEFfilename, SchemaTypeIDConstants.Schema_Standard_Exchange_Format
        
        'instantiates XML object
        Set oXmlDoc = oEdiDoc.GetXmlDocument
    
        'Load XML file
        oXmlDoc.LoadEdi mXMLfilename
    
        If Trim(mEDIfilename) = "" Then
            mEDIfilename = mPath & "Output_EDIfile.X12"
        End If
        'Save as EDI file
        oEdiDoc.Save mEDIfilename
    
        MsgBox "Done. File output = " + mEDIfilename
    End If
End Sub

Private Sub Form_Load()
    mPath = Trim(App.Path) & "\"
    cmdEDI2XML.Enabled = False
    cmdXML2EDI.Enabled = False
End Sub

Private Sub optEDI2XML_Click()
    disableEnable
End Sub

Private Sub optEDI2XML_Validate(Cancel As Boolean)
    disableEnable
End Sub

Private Sub optXML2EDI_Click()
    disableEnable
End Sub

Private Sub optXML2EDI_Validate(Cancel As Boolean)
    disableEnable
End Sub

Private Function disableEnable()
    If optXML2EDI.Value = True Then
        txtEDIfileName.Enabled = False
        cmdattachEDIfile.Enabled = False
        cmdEDI2XML.Enabled = False
        cmdXML2EDI.Enabled = True
        txtXMLfileName.Enabled = True
        cmdattachXMLfile.Enabled = True
    ElseIf optEDI2XML.Value = True Then
        txtXMLfileName.Enabled = False
        cmdattachXMLfile.Enabled = False
        cmdEDI2XML.Enabled = True
        cmdXML2EDI.Enabled = False
        txtEDIfileName.Enabled = True
        cmdattachEDIfile.Enabled = True
    End If
End Function
